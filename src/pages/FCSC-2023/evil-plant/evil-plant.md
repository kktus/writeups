---
title: "FCSC-2023 - Misc - Evil Plant"
slug: "/fcsc-2023/evil-plant"
date: "2023-05-01"
tags: ["FCSC", "FCSC 2023", "OPCUA", "OT", "Python"]
---

In this challenge, we need to communicate with an OPCUA Server of a fictive company that creates a secret formula
and we need to find it.

In OPCUA, we can register to the different objects and be notified upon change. 
Thus, we create a script that listen to all the modifications of the plant in order to find whats happening in real time.

Here is my solution using the opcua library that keep the modifications in the PLC components and create the flag:

```python
import time
from opcua import Client

NODES = {}
RESET = False
MEMORY = []
SERVER = "opc.tcp://evil-plant.france-cybersecurity-challenge.fr:4841"


class SubHandler(object):
    def datachange_notification(self, node, val, data):
        global RESET
        global MEMORY
        name = NODES[node]["name"]

        if name == "mix_element" and val == 0:
            print("Reset")
            if RESET:
                print(MEMORY)
                flag = ""
                for (e1, v1), (e2, v2) in zip(*[iter(MEMORY)] * 2):
                    print(e1, v1, e2, v2)
                    flag += f"{e1:02x}{e2:02x}{v1:02x}{v2:02x}"
                print("FCSC{" + flag + "}")
            RESET = True

        if RESET:
            if name[0] == "E":
                num = int(name[1:])
                units = NODES[node]["val"] - val
                print(f"Add {units} of {num}")
                MEMORY.append((num, units))

        NODES[node]["val"] = val


client = Client(SERVER)
client.connect()
root = client.get_root_node()
nodes = root.get_children()[0].get_children()
handler = SubHandler()
sub = client.create_subscription(500, handler)
for node in nodes:
    NODES[node] = {
        "name": node.get_display_name().Text
    }

    try:
        handle = sub.subscribe_data_change(node)
    except:  # Sometimes we cannot register to a component
        pass

try:  # As the notification handler is started on another thread, we to keep the main thread
    time.sleep(40000)
finally:  # Be nice, disconnect
    client.disconnect()
```

Output:
```
Reset
Add 1 of 1
Add 193 of 3
Add 56 of 7
Add 29 of 11
Add 201 of 2
Add 238 of 13
Add 141 of 6
Add 199 of 15
Add 200 of 8
Add 231 of 12
Add 145 of 4
Add 139 of 16
Add 183 of 9
Add 176 of 10
Add 230 of 5
Add 143 of 14
Reset
[(1, 1), (3, 193), (7, 56), (11, 29), (2, 201), (13, 238), (6, 141), (15, 199), (8, 200), (12, 231), (4, 145), (16, 139), (9, 183), (10, 176), (5, 230), (14, 143)]
1 1 3 193
7 56 11 29
2 201 13 238
6 141 15 199
8 200 12 231
4 145 16 139
9 183 10 176
5 230 14 143
FCSC{...}
```