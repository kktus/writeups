---
title: "FCSC-2023 - Hardware - Fibonacci"
slug: "/fcsc-2023/fibonacci"
date: "2023-05-01"
tags: ["FCSC", "FCSC 2023", "Hardware", "Assembly"]
---

This challenge consisted in creating a program that generates Fibonacci suite in modified assembly.

Here is my answer:
```
MOV R0, #0x0 -- Init 0
MOV R1, #0x1 -- Init 1

CMP R5, R0 -- If R5 == 0, end
JNZA loop
STP

loop:  -- Main Loop

ADD R2, R0, R1 -- Compute
MOV R0, R1  -- R0 = R1
MOV R1, R2  -- R1 = R2

-- 3 following lines: DECR R5
MOV R4, #0x1 
MOV R3, R5
SUB R5, R3, R4 

-- Loop while R5 > 0
MOV R9, #0x0
CMP R5, R9
JNZA loop

STP
```