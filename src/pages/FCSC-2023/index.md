---
title: "FCSC-2023 - Summary"
slug: "/fcsc-2023"
date: "2023-05-01"
tags: ["FCSC", "FCSC 2023"]
---

Place: `90th`
Points: `3051`

![Solves](./solves.png)

Writeups
--------
- [Hardware] [Canflag](/fcsc-2023/canflag) - 159pts
- [Hardware] [Fibonnaci](/fcsc-2023/fibonacci) - 178pts
- [Hardware] [Au boolot](/fcsc-2023/au-boolot) - 365pts
- [Misc] [Tri très selectif](/fcsc-2023/tri-tres-selectif) - 103pts
- [Misc] [Des p'tits trous](/fcsc-2023/des-ptits-trous) - 298pts
- [Misc] [Evil Plant](/fcsc-2023/evil-plant) - 406pts
- [Side Channel and Fault Attacks] [Lapin Blanc](/fcsc-2023/lapin-blanc) - 176pts
- [Web] [Hello from the inside](/fcsc-2023/hello-from-the-inside) - 296pts
- [Web] [Tweedle Dum](/fcsc-2023/tweedle-dum) - 367pts


It was fun, thank you =)


