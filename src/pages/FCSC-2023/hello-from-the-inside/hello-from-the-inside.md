---
title: "FCSC-2023 - Web - Hello from the inside"
slug: "/fcsc-2023/hello-from-the-inside"
date: "2023-05-01"
tags: ["FCSC", "FCSC 2023", "Web", "Apache"]
---

Here is the app:
![img.png](img.png)

When inspecting the request sent to the backend we can see that there is a hidden parameter: `service`
When putting `127.0.0.1` as service, we can see that the service loads the page itself. 

We can then try to load a page that is normally restricted in apache, for example: `/server-status`:
![Find the secret page](secret-page.png)

Luckily this endpoint leaks a path to a secret admin page, that we can include to find the flag:
![img_1.png](img_1.png)