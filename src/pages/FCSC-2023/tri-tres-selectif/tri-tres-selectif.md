---
title: "FCSC-2023 - Misc - Tri très sélectif"
slug: "/fcsc-2023/tri-tres-selectif"
date: "2023-05-01"
tags: ["FCSC", "FCSC 2023", "Misc", "Python"]
---

```
$ nc challenges.france-cybersecurity-challenge.fr 2052
Votre but est de trier un tableau dont vous ne voyez pas les valeurs (chacune est remplacée par *) :
* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
Actions possibles:
  - "comparer X Y": compare les valeurs du tableau aux cases X et Y, et retourne 1 si la valeur en X est inférieure ou égale à celle en Y, 0 sinon.
  - "echanger X Y": échange les valeurs du tableau aux cases X et Y, et affiche le taleau modifié.
  - "longueur:       retourne la longueur du tableau.
  - "verifier:      retourne le flag si le tableau est trié.
```

In this challenge, we need to find a way to sort an array using those primitives functions (swap, compare). 
Luckily those are pretty standard and are the base in most of the known sort algorithms.

However, if we use a simple approach (like select search), it will not work as we have a limited number of possible comparisons.

One of the most efficient way to sort an array is to do a Quick Sort (https://en.wikipedia.org/wiki/Quicksort). 
We can find the algorithm on the internet and modify it to fit the challenge:

```python
import socket

HOST = "challenges.france-cybersecurity-challenge.fr"
PORT = 2052


def recv_until(mark=">>>"):
    data = ""
    while mark not in data:
        new_data = s.recv(1024).decode()
        if new_data is "":
            break
        data += new_data

    print(f"RCV {data}")
    return data.replace(">>>", "")


def send(data):
    print(f"SND {data}")
    s.send(data.encode())


memory = {}
with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
    s.connect((HOST, PORT))
    recv_until()
    send("longueur\n")
    longueur = int(recv_until())


    def compare(a, b):
        send("comparer %i %i\n" % (a, b))
        return int(recv_until())


    def swap(a, b):
        send("echanger %i %i\n" % (a, b))
        recv_until()


    """ Bonus: select sort from Intro - Tri Selectif
    for i in range(longueur-1):
    min_val = i
    for j in range(i, longueur):
        if not compare(min_val, j):
            min_val = j
    if min_val != i:
        swap(min_val, i)
    """

    def partition(left, right):
        pivot = left + (right - left) / 2
        swap(left, pivot)
        pivot = left
        left += 1

        while right >= left:
            while left <= right and compare(left, pivot):
                left += 1
            while left <= right and not compare(right, pivot):
                right -= 1

            if left <= right:
                swap(left, right)
                left += 1
                right -= 1
            else:
                break

        swap(right, pivot)

        return right

    def quicksort(left, right):
        if left >= right:
            return
        if right - left == 1:
            if not compare(left, right):
                swap(left, right)
                return

        pivot = partition(left, right)

        quicksort(left, pivot - 1)
        quicksort(pivot + 1, right)


    quicksort(0, longueur - 1)
    send("verifier\n")
    recv_until()
```