---
title: "FCSC-2023 - Hardware - canflag "
slug: "/fcsc-2023/canflag"
date: "2023-05-01"
tags: ["FCSC", "FCSC 2023", "Hardware", "Programming", "CAN", "Python"]
---

In this challenge we had a network capture containing a capture of CAN protocol.
Only three fields changes between packegs: `id`, `data` and `extra flag`.

The id seems to appear twice for each paquet, one with the extra flag set and one without. 

We can then use advanced guessing techniques to find that we can sort the data by `id` as first value and `extra flag` as second.

We can extract this data using tshark: `tshark -r canflag.pcap -T fields -e can.id  -e can.flags.xtd -e data` and use a script to rebuild the flag:

```python
import binascii

# Extracted using:
# tshark -r canflag.pcap -T fields -e can.id  -e can.flags.xtd -e data
data = """
56      1
30      1       31643861
38      1       32
20      0
32      1
54      0       64
62      1
66      0
8       1
40      0       30316335306230
46      0
28      0       666662656239636139326163
68      1       61393665656633377d
18      0
60      0       3739373239356333636335383735
2       0       7b61613965
22      0       3630336436613131
60      1
44      1       613738653638323535
48      1       63
8       0
10      1
14      0       3865
54      1
20      1       35
42      1
34      0       323764
52      1       3266363063
6       0       6630613265613565323333
30      0       656533636439313931383461
4       0
22      1       6636623437623966
42      0       6535376265366131353630
50      1       6536363063653731663665323130
16      1
36      1
58      0       33396536306336
38      0       64643466
36      0       36616664
12      0       343536646637646234
34      1
50      0       3133643166323438
24      0       6234
4       1       64
16      0
12      1       636330353561
28      1       31
62      0       376264656161
26      1
56      0       6263313466333730323062356134
10      0       6264633366633439353335613039
24      1       37316432306231353435386237
46      1
18      1
64      1
14      1       6162
32      0
44      0       36
26      0       346163
52      0       65313733326535393833
66      1       3835
2       1       46435343
64      0
40      1
58      1
48      0       35323966373464386230346638
6       1
"""

r = []
for line in data.split("\n"):
    f = [k for k in line.split(" ") if k != ""]
    if len(f) == 3:  # Full line (with data)
        r.append((
            int(f[0]),  # First sort argument, can ID
            1 - int(f[1]),  # Second sort argument, extra flag
            binascii.unhexlify(f[2]).decode() # data
        ))

r.sort()

print("".join(c[2] for c in r))
```