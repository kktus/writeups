import React from "react"
import { Link, graphql } from "gatsby"

import Layout from "../components/layout"
import Seo from "../components/seo"

const IndexPage = () => (
  <Layout>
    <Seo title="Home" />
    <h1>Hi, here is my list of writeups</h1>
    <Link to="/404ctf-2023">404ctf-2023</Link><br />
    <Link to="/fcsc-2023">FCSC-2023</Link>

  </Layout>
)

export default IndexPage

