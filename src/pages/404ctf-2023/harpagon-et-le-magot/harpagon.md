---
title: "404ctf-2023 - Cloud - Harpagon et le magot"
slug: "/404ctf-2023/harpagon-et-le-magot"
date: "2023-06-05"
tags: ["404ctf", "404ctf 2023", "Cloud", "Kubernetes"]
---

![Description](./description.png)

First, we can take a look at the history
```bash
harpagon@jardin:~$ history
1  curl -sfL https://get.k3s.io | sh -
2  echo 'export KUBECONFIG=.kube/config' > .bashrc
3  mkdir ~/.kube
4  sudo k3s kubectl config view --raw > .kube/config
5  chmod 600 .kube/config
6  . .bashrc
7  kubectl cluster-info
8  sudo curl https://raw.githubusercontent.com/helm/helm/main/scripts/get-helm-3 | bash
9  kubectl create namespace cassette
10  kubectl config set-context --current --namespace cassette
11  sudo apt install git
12  git clone https://github.com/guerzon/vaultwarden
13  cp vaultwarden/values.yaml values.yaml
14  vim values.yaml
15  helm install cassette vaultwarden --values values.yaml --set ingress.enabled=true
16  vim values.yaml
17  helm install cassette vaultwarden --values values.yaml --set ingress.enabled=true
18  helm upgrade cassette vaultwarden --values values.yaml --set ingress.enabled=true
19  history
```

Then, we can take a look at the values of the app:
```
harpagon@jardin:~$ cat values.yaml
...
adminToken: "Yh6UxUhujjl4UqBrIJFtsWM8xTbvvT" # Bas les pattes, voleur, mon secret ne se trouve plus ici !
...
```

In history line 14 administrator filled the values.yaml file with the flag.
In line 15, he deployed the app and changed the flag to the new value (that we found) on line 16.

Line 17 (helm install) didn't worked because app was already deployed, but then the application has been modified using the new values in line 18.

This challenge consists in recovering the flag located in the first version of helm deployment, before upgrade.

OK, now one important thing, NEVER STORE SECRETS IN HELM VALUES.

Do you know why ? 

Because helm values are versioned, you can find the history of all changes of values.
This is really useful if you upgrade your helm app, cause a crash, and then want to restore.

However, previous secrets can be recovered:
```bash
harpagon@jardin:~$ helm get values cassette --revision 1
USER-SUPPLIED VALUES:
adminToken: '}--DETCADER---{FTC404'
...
```