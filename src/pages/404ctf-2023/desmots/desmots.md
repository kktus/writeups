---
title: "404ctf-2023 - Programmation - Des mots, des mots, des mots "
slug: "/404ctf-2023/des-mots"
date: "2023-06-05"
tags: ["404ctf", "404ctf 2023", "Programming", "Python"]
---

![Description](./description.png)

This challenge was not really difficult but the main complexity was to understand
the rules.

```python
import socket
import string

HOST = "challenges.404ctf.fr"
PORT = 30980

ALPHABET = string.ascii_letters + string.ascii_uppercase
VOYELLE = "aeiouyAEIOUY"
CONSONNE = ALPHABET
for k in VOYELLE:
    CONSONNE = CONSONNE.replace(k, "")


def recv_until(mark=">>>"):
    data = ""
    while new_data := soc.recv(1024).decode():
        data += new_data
        if mark in data:
            break
    print(f"RCV {data}")
    return data.replace(">>>", "")


def send(data):
    print(f"SND {data}")
    soc.send((data + "\n").encode())


def regle1(value):
    # Inverser les lettres
    print("REGLE 1", value)
    return value[::-1]


def regle2(value):
    """
    Règle 2 :
    - Si le mot à un nombre de lettres pair, échanger la 1ere et la 2e partie du mot obtenu
    - Sinon, enlever toutes les lettres du mot correspondant à la lettre centrale
    """
    print("REGLE 2", value)

    if len(value) % 2 == 0:
        return value[int(len(value) / 2):] + value[:int(len(value) / 2)]

    return value.replace(value[int(len(value) / 2)], "")


def regle3(original, value):
    print("REGLE 3", value)
    """
    Si le mot a 3 lettres ou plus :
    - Si la 3e lettre du mot obtenu est une consonne, "décaler" les voyelles vers la gauche dans le mot original, puis réappliquer les règles 1 et 2.
    - Sinon : la même chose mais les décaler vers la droite.
    > Ex de décalage : poteau => petauo // drapeau => drupaea
    """
    if len(value) >= 3:
        original = list(k for k in original)
        lasti = mem = -1

        for i in range(len(original) - 1, -1, -1) \
                if value[2] in CONSONNE \
                else range(len(original)):
            if original[i] in VOYELLE:
                if lasti == -1:
                    lasti, mem = i, original[i]
                else:
                    mem, original[i] = original[i], mem

        if mem != -1:
            original[lasti] = mem

        original = "".join(original)
        original = regle1(original)
        original = regle2(original)
        return original

    return value


def regle4(value):
    print("REGLE 4", value)
    """
    Règle 4 :
    - Pour `n` allant de 0 à la fin du mot, si le caractère `c` à la position `n` du mot est une consonne (majuscule ou minuscule), 
    insérer en position `n+1` le caractère de code ASCII `a = ((vp + s) % 95) + 32`, 
    où `vp` est le code ASCII de la voyelle précédant la consonne `c` dans l'alphabet (si `c = 'F'`, `vp = 'E'`), 
    et `s = SOMME{i=n-1 -> 0}(a{i}*2^(n-i)*Id(l{i} est une voyelle))`, où `a{i}` est le code ASCII de la `i`-ième lettre du mot, 
    `Id(x)` vaut `1` si `x` est vrai, `0` sinon, et `l{i}` la `i`-ième lettre du mot. 
    
    Attention à bien appliquer cette règle aussi sur les caractères insérés au mot.
    
    > Ex : futur => f&ut\\ur@
    
    - Enfin, trier le mot par ordre décroissant d'occurrences des caractères, 
      puis par ordre croissant en code ASCII pour les égalités
    
    > Ex de tri : patate => aattep
    Entrée : {cosette}
    """
    value = list(k for k in value)

    n = 0
    while n < len(value):
        c = value[n]

        if c in CONSONNE:

            pos_voy = ALPHABET.index(c)
            while ALPHABET[pos_voy] not in VOYELLE:
                pos_voy -= 1
            vp = ord(ALPHABET[pos_voy]) # code ASCII de la voyelle précédant la consonne

            s = sum(
                ord(value[i]) * 2 ** (n - i) * (1 if value[i] in VOYELLE else 0)
                for i in range(n - 1, -1, -1)
            ) # s = SOMME{i=n-1 -> 0}(a{i}*2^(n-i)*Id(l{i} est une voyelle))

            a = ((vp + s) % 95) + 32
            value.insert(n + 1, chr(a))

        n += 1

    # trier le mot par ordre décroissant d'occurrences des caractères
    value = sorted(value, key=lambda k: (-value.count(k), ord(k)))

    return "".join(value)


with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as soc:
    soc.connect((HOST, PORT))

    while True:
        regle = entree = -1
        data = recv_until(">> ")
        for line in data.split("\n"):
            if "Règle " in line:
                regle = int(line.split(":")[0].strip().lstrip("Règle "))
            elif "Entrée" in line:
                entree = line.split("{")[1].split("}")[0]

        print("----------------")
        print("Entrée:", entree)
        print("Regle:", regle)

        if regle == 0:
            pass

        elif regle == 1:
            entree = regle1(entree)

        elif regle == 2:
            entree = regle1(entree)
            entree = regle2(entree)

        elif regle == 3:
            orig = entree
            entree = regle1(entree)
            entree = regle2(entree)
            entree = regle3(orig, entree)

        elif regle == 4:
            orig = entree
            entree = regle1(entree)
            entree = regle2(entree)
            entree = regle3(orig, entree)
            entree = regle4(entree)

        else:
            total = []
            for word in entree.split():
                orig = word
                word = regle1(word)
                word = regle2(word)
                word = regle3(orig, word)
                word = regle4(word)
                total.append(word)
            entree = " ".join(total)

        send(entree)
        print("----------------")
```

Solves trace:
```
RCV Commençons. Je te propose de démarrer en transformant mon nom.
Tout d'abord retourne mon nom sans modifications.
Règle 0 : Aucune modification
Entrée : {cosette}
>> 
----------------
Entrée: cosette
Regle: 0
SND cosette
----------------
RCV Je vois que tu as compris. La première règle de ce langage est très simple.
Règle 1 : Inverser les lettres
Entrée : {cosette}
>> 
----------------
Entrée: cosette
Regle: 1
REGLE 1 cosette
SND ettesoc
----------------
RCV Oui c'est bien. Maintenant la deuxième règle est un peu plus difficile.
Règle 2 :
- Si le mot à un nombre de lettres pair, échanger la 1ere et la 2e partie du mot obtenu
- Sinon, enlever toutes les lettres du mot correspondant à la lettre centrale
Entrée : {cosette}
>> 
----------------
Entrée: cosette
Regle: 2
REGLE 1 cosette
REGLE 2 ettesoc
SND ttsoc
----------------
RCV Tu t'en sors très bien ! Continuons avec la troisième règle.
Règle 3 :
_Si le mot a 3 lettres ou plus_ :

- Si la 3e lettre du mot obtenu est une consonne, "décaler" les voyelles vers la gauche dans le mot original, puis réappliquer les règles 1 et 2.
- Sinon : la même chose mais les décaler vers la droite.

> Ex de décalage : _poteau => petauo_ // _drapeau => drupaea_
Entrée : {cosette}
>> 
----------------
Entrée: cosette
Regle: 3
REGLE 1 cosette
REGLE 2 ettesoc
REGLE 3 ttsoc
REGLE 1 cesetto
REGLE 2 ottesec
SND ottsc
----------------
RCV Nous avons presque fini, la quatrième règle est la plus complexe.
Règle 4 :
- Pour `n` allant de 0 à la fin du mot, si le caractère `c` à la position `n` du mot est une consonne (majuscule ou minuscule), insérer en position `n+1` le caractère de code ASCII `a = ((vp + s) % 95) + 32`, où `vp` est le code ASCII de la voyelle précédant la consonne `c` dans l'alphabet (si `c = 'F'`, `vp = 'E'`), et `s = SOMME{i=n-1 -> 0}(a{i}*2^(n-i)*Id(l{i} est une voyelle))`, où `a{i}` est le code ASCII de la `i`-ième lettre du mot, `Id(x)` vaut `1` si `x` est vrai, `0` sinon, et `l{i}` la `i`-ième lettre du mot. _Attention à bien appliquer cette règle aussi sur les caractères insérés au mot._

> Ex : _futur => f&ut\\ur@_

- Enfin, trier le mot par ordre décroissant d'occurrences des caractères, puis par ordre croissant en code ASCII pour les égalités

> Ex de tri : _patate => aattep_
Entrée : {cosette}
>> 
----------------
Entrée: cosette
Regle: 4
REGLE 1 cosette
REGLE 2 ettesoc
REGLE 3 ttsoc
REGLE 1 cesetto
REGLE 2 ottesec
REGLE 4 ottsc
SND PPtt!15QRUWcos
----------------
RCV Bravo ! Maintenant je vais te donner un chapitre dont j'ai besoin de la traduction complète.
Chaque mot est écrit en minuscule sans accents ni caractères spéciaux et sont séparés par un espace. Tu as 5 secondes pour répondre.
Entrée : {cependant tandis quelques chantaient autres causaient tumultueusement ensemble etait bruit tholomyes intervint parlons point hasard ecria meditons voulons eblouissants improvisation betement esprit biere coule amasse point mousse messieurs melons majeste ripaille mangeons recueillement festinons lentement pressons voyez printemps depeche flambe exces pechers abricotiers exces grace diners messieurs grimod reyniere talleyrand sourde rebellion gronda groupe tholomyes laisse tranquilles blachevelle tyran fameuil bombarda bombance bamboche listolier dimanche existe reprit fameuil sommes sobres ajouta listolier tholomyes blachevelle contemple calme marquis repondit tholomyes mediocre effet pierre marquis montcalm etait royaliste alors celebre toutes grenouilles turent ecria tholomyes accent homme ressaisit empire remettez stupeur accueille}
>> 
----------------
Entrée: cependant tandis quelques chantaient autres causaient tumultueusement ensemble etait bruit tholomyes intervint parlons point hasard ecria meditons voulons eblouissants improvisation betement esprit biere coule amasse point mousse messieurs melons majeste ripaille mangeons recueillement festinons lentement pressons voyez printemps depeche flambe exces pechers abricotiers exces grace diners messieurs grimod reyniere talleyrand sourde rebellion gronda groupe tholomyes laisse tranquilles blachevelle tyran fameuil bombarda bombance bamboche listolier dimanche existe reprit fameuil sommes sobres ajouta listolier tholomyes blachevelle contemple calme marquis repondit tholomyes mediocre effet pierre marquis montcalm etait royaliste alors celebre toutes grenouilles turent ecria tholomyes accent homme ressaisit empire remettez stupeur accueille
Regle: -1
REGLE 1 cependant
REGLE 2 tnadnepec
REGLE 3 tadepec
REGLE 1 cepandent
REGLE 2 tnednapec
REGLE 4 tedapec
REGLE 1 tandis
REGLE 2 sidnat
REGLE 3 natsid
REGLE 1 tindas
REGLE 2 sadnit
REGLE 4 nitsad
REGLE 1 quelques
REGLE 2 seuqleuq
REGLE 3 leuqseuq
REGLE 1 qeulqeus
REGLE 2 sueqlueq
REGLE 4 lueqsueq
REGLE 1 chantaient
REGLE 2 tneiatnahc
REGLE 3 tnahctneia
REGLE 1 chentaaint
REGLE 2 tniaatnehc
REGLE 4 tnehctniaa
REGLE 1 autres
REGLE 2 sertua
REGLE 3 tuaser
REGLE 1 eatrus
REGLE 2 surtae
REGLE 4 taesur
REGLE 1 causaient
REGLE 2 tneiasuac
REGLE 3 tneisuc
REGLE 1 ceasuaint
REGLE 2 tniausaec
REGLE 4 tniasaec
REGLE 1 tumultueusement
REGLE 2 tnemesueutlumut
REGLE 3 tnmsuutlumut
REGLE 1 tumulteuesemunt
REGLE 2 tnumeseuetlumut
REGLE 4 tnmeseetlmt
REGLE 1 ensemble
REGLE 2 elbmesne
REGLE 3 esneelbm
REGLE 1 ensemble
REGLE 2 elbmesne
REGLE 4 esneelbm
REGLE 1 etait
REGLE 2 tiate
REGLE 3 tite
REGLE 1 atiet
REGLE 2 teita
REGLE 4 teta
REGLE 1 bruit
REGLE 2 tiurb
REGLE 3 tirb
REGLE 1 briut
REGLE 2 tuirb
REGLE 4 turb
REGLE 1 tholomyes
REGLE 2 seymoloht
REGLE 3 seymlht
REGLE 1 thelomoys
REGLE 2 syomoleht
REGLE 4 symleht
REGLE 1 intervint
REGLE 2 tnivretni
REGLE 3 tnivetni
REGLE 1 intirvent
REGLE 2 tnevritni
REGLE 4 tnevitni
REGLE 1 parlons
REGLE 2 snolrap
REGLE 3 snorap
REGLE 1 porlans
REGLE 2 snalrop
REGLE 4 snarop
REGLE 1 point
REGLE 2 tniop
REGLE 3 tnop
REGLE 1 piont
REGLE 2 tnoip
REGLE 4 tnip
REGLE 1 hasard
REGLE 2 drasah
REGLE 3 sahdra
REGLE 1 hasard
REGLE 2 drasah
REGLE 4 sahdra
REGLE 1 ecria
REGLE 2 airce
REGLE 3 aice
REGLE 1 icrae
REGLE 2 earci
REGLE 4 eaci
REGLE 1 meditons
REGLE 2 snotidem
REGLE 3 idemsnot
REGLE 1 modetins
REGLE 2 snitedom
REGLE 4 edomsnit
REGLE 1 voulons
REGLE 2 snoluov
REGLE 3 snouov
REGLE 1 vooluns
REGLE 2 snuloov
REGLE 4 snuoov
REGLE 1 eblouissants
REGLE 2 stnassiuolbe
REGLE 3 iuolbestnass
REGLE 1 ableoussints
REGLE 2 stnissuoelba
REGLE 4 uoelbastniss
REGLE 1 improvisation
REGLE 2 noitasivorpmi
REGLE 3 notasvorpm
REGLE 1 omprivasitoin
REGLE 2 niotisavirpmo
REGLE 4 niotisvirpmo
REGLE 1 betement
REGLE 2 tnemeteb
REGLE 3 etebtnem
REGLE 1 betement
REGLE 2 tnemeteb
REGLE 4 etebtnem
REGLE 1 esprit
REGLE 2 tirpse
REGLE 3 psetir
REGLE 1 ispret
REGLE 2 terpsi
REGLE 4 psiter
REGLE 1 biere
REGLE 2 ereib
REGLE 3 rib
REGLE 1 beeri
REGLE 2 ireeb
REGLE 4 irb
REGLE 1 coule
REGLE 2 eluoc
REGLE 3 eloc
REGLE 1 ceolu
REGLE 2 uloec
REGLE 4 ulec
REGLE 1 amasse
REGLE 2 essama
REGLE 3 amaess
REGLE 1 emassa
REGLE 2 assame
REGLE 4 ameass
REGLE 1 point
REGLE 2 tniop
REGLE 3 tnop
REGLE 1 piont
REGLE 2 tnoip
REGLE 4 tnip
REGLE 1 mousse
REGLE 2 essuom
REGLE 3 uomess
REGLE 1 muesso
REGLE 2 osseum
REGLE 4 eumoss
REGLE 1 messieurs
REGLE 2 srueissem
REGLE 3 sruessem
REGLE 1 musseiers
REGLE 2 sreiessum
REGLE 4 srissum
REGLE 1 melons
REGLE 2 snolem
REGLE 3 lemsno
REGLE 1 molens
REGLE 2 snelom
REGLE 4 lomsne
REGLE 1 majeste
REGLE 2 etsejam
REGLE 3 tsjam
REGLE 1 mejesta
REGLE 2 atsejem
REGLE 4 atsjm
REGLE 1 ripaille
REGLE 2 elliapir
REGLE 3 apirelli
REGLE 1 repialli
REGLE 2 illaiper
REGLE 4 iperilla
REGLE 1 mangeons
REGLE 2 snoegnam
REGLE 3 gnamsnoe
REGLE 1 mongaens
REGLE 2 sneagnom
REGLE 4 gnomsnea
REGLE 1 recueillement
REGLE 2 tnemellieucer
REGLE 3 tnemeieucer
REGLE 1 receuelliment
REGLE 2 tnemilleuecer
REGLE 4 tnemieuecer
REGLE 1 festinons
REGLE 2 snonitsef
REGLE 3 snontsef
REGLE 1 fostenins
REGLE 2 sninetsof
REGLE 4 snintsof
REGLE 1 lentement
REGLE 2 tnemetnel
REGLE 3 tnmtnl
REGLE 1 lentement
REGLE 2 tnemetnel
REGLE 4 tnmtnl
REGLE 1 pressons
REGLE 2 snosserp
REGLE 3 serpsnos
REGLE 1 prossens
REGLE 2 snessorp
REGLE 4 sorpsnes
REGLE 1 voyez
REGLE 2 zeyov
REGLE 3 zeov
REGLE 1 veoyz
REGLE 2 zyoev
REGLE 4 zyev
REGLE 1 printemps
REGLE 2 spmetnirp
REGLE 3 spmenirp
REGLE 1 prentimps
REGLE 2 spmitnerp
REGLE 4 spminerp
REGLE 1 depeche
REGLE 2 ehceped
REGLE 3 hcpd
REGLE 1 depeche
REGLE 2 ehceped
REGLE 4 hcpd
REGLE 1 flambe
REGLE 2 ebmalf
REGLE 3 alfebm
REGLE 1 flemba
REGLE 2 abmelf
REGLE 4 elfabm
REGLE 1 exces
REGLE 2 secxe
REGLE 3 sexe
REGLE 1 exces
REGLE 2 secxe
REGLE 4 sexe
REGLE 1 pechers
REGLE 2 srehcep
REGLE 3 srecep
REGLE 1 pechers
REGLE 2 srehcep
REGLE 4 srecep
REGLE 1 abricotiers
REGLE 2 sreitocirba
REGLE 3 sreitcirba
REGLE 1 ebracitoirs
REGLE 2 srioticarbe
REGLE 4 srotcarbe
REGLE 1 exces
REGLE 2 secxe
REGLE 3 sexe
REGLE 1 exces
REGLE 2 secxe
REGLE 4 sexe
REGLE 1 grace
REGLE 2 ecarg
REGLE 3 ecrg
REGLE 1 greca
REGLE 2 acerg
REGLE 4 acrg
REGLE 1 diners
REGLE 2 srenid
REGLE 3 nidsre
REGLE 1 denirs
REGLE 2 srined
REGLE 4 nedsri
REGLE 1 messieurs
REGLE 2 srueissem
REGLE 3 sruessem
REGLE 1 musseiers
REGLE 2 sreiessum
REGLE 4 srissum
REGLE 1 grimod
REGLE 2 domirg
REGLE 3 irgdom
REGLE 1 gromid
REGLE 2 dimorg
REGLE 4 orgdim
REGLE 1 reyniere
REGLE 2 ereinyer
REGLE 3 nyererei
REGLE 1 reenyire
REGLE 2 eriyneer
REGLE 4 neereriy
REGLE 1 talleyrand
REGLE 2 dnaryellat
REGLE 3 ellatdnary
REGLE 1 tellyarand
REGLE 2 dnarayllet
REGLE 4 ylletdnara
REGLE 1 sourde
REGLE 2 edruos
REGLE 3 uosedr
REGLE 1 suerdo
REGLE 2 odreus
REGLE 4 eusodr
REGLE 1 rebellion
REGLE 2 noilleber
REGLE 3 noieber
REGLE 1 robellein
REGLE 2 niellebor
REGLE 4 nieebor
REGLE 1 gronda
REGLE 2 adnorg
REGLE 3 orgadn
REGLE 1 grando
REGLE 2 odnarg
REGLE 4 argodn
REGLE 1 groupe
REGLE 2 epuorg
REGLE 3 orgepu
REGLE 1 gruepo
REGLE 2 opeurg
REGLE 4 urgope
REGLE 1 tholomyes
REGLE 2 seymoloht
REGLE 3 seymlht
REGLE 1 thelomoys
REGLE 2 syomoleht
REGLE 4 symleht
REGLE 1 laisse
REGLE 2 essial
REGLE 3 ialess
REGLE 1 liessa
REGLE 2 asseil
REGLE 4 eilass
REGLE 1 tranquilles
REGLE 2 selliuqnart
REGLE 3 selliqnart
REGLE 1 trunqiellas
REGLE 2 salleiqnurt
REGLE 4 salleqnurt
REGLE 1 blachevelle
REGLE 2 ellevehcalb
REGLE 3 llvhcalb
REGLE 1 blechevella
REGLE 2 allevehcelb
REGLE 4 allvhclb
REGLE 1 tyran
REGLE 2 naryt
REGLE 3 nayt
REGLE 1 taryn
REGLE 2 nyrat
REGLE 4 nyat
REGLE 1 fameuil
REGLE 2 liuemaf
REGLE 3 liumaf
REGLE 1 fimaeul
REGLE 2 lueamif
REGLE 4 luemif
REGLE 1 bombarda
REGLE 2 adrabmob
REGLE 3 bmobadra
REGLE 1 bamborda
REGLE 2 adrobmab
REGLE 4 bmabadro
REGLE 1 bombance
REGLE 2 ecnabmob
REGLE 3 bmobecna
REGLE 1 bembonca
REGLE 2 acnobmeb
REGLE 4 bmebacno
REGLE 1 bamboche
REGLE 2 ehcobmab
REGLE 3 bmabehco
REGLE 1 bembacho
REGLE 2 ohcabmeb
REGLE 4 bmebohca
REGLE 1 listolier
REGLE 2 reilotsil
REGLE 3 reiltsil
REGLE 1 lestiloir
REGLE 2 riolitsel
REGLE 4 roltsel
REGLE 1 dimanche
REGLE 2 ehcnamid
REGLE 3 amidehcn
REGLE 1 demincha
REGLE 2 ahcnimed
REGLE 4 imedahcn
REGLE 1 existe
REGLE 2 etsixe
REGLE 3 ixeets
REGLE 1 exesti
REGLE 2 itsexe
REGLE 4 exeits
REGLE 1 reprit
REGLE 2 tirper
REGLE 3 pertir
REGLE 1 ripret
REGLE 2 terpir
REGLE 4 pirter
REGLE 1 fameuil
REGLE 2 liuemaf
REGLE 3 liumaf
REGLE 1 fimaeul
REGLE 2 lueamif
REGLE 4 luemif
REGLE 1 sommes
REGLE 2 semmos
REGLE 3 mossem
REGLE 1 semmos
REGLE 2 sommes
REGLE 4 messom
REGLE 1 sobres
REGLE 2 serbos
REGLE 3 bosser
REGLE 1 sebros
REGLE 2 sorbes
REGLE 4 bessor
REGLE 1 ajouta
REGLE 2 atuoja
REGLE 3 ojaatu
REGLE 1 ajaotu
REGLE 2 utoaja
REGLE 4 ajauto
REGLE 1 listolier
REGLE 2 reilotsil
REGLE 3 reiltsil
REGLE 1 lestiloir
REGLE 2 riolitsel
REGLE 4 roltsel
REGLE 1 tholomyes
REGLE 2 seymoloht
REGLE 3 seymlht
REGLE 1 thelomoys
REGLE 2 syomoleht
REGLE 4 symleht
REGLE 1 blachevelle
REGLE 2 ellevehcalb
REGLE 3 llvhcalb
REGLE 1 blechevella
REGLE 2 allevehcelb
REGLE 4 allvhclb
REGLE 1 contemple
REGLE 2 elpmetnoc
REGLE 3 lpmtnoc
REGLE 1 centemplo
REGLE 2 olpmetnec
REGLE 4 olpmtnc
REGLE 1 calme
REGLE 2 emlac
REGLE 3 emac
REGLE 1 celma
REGLE 2 amlec
REGLE 4 amec
REGLE 1 marquis
REGLE 2 siuqram
REGLE 3 siuram
REGLE 1 mirqaus
REGLE 2 suaqrim
REGLE 4 suarim
REGLE 1 repondit
REGLE 2 tidnoper
REGLE 3 opertidn
REGLE 1 ripendot
REGLE 2 todnepir
REGLE 4 epirtodn
REGLE 1 tholomyes
REGLE 2 seymoloht
REGLE 3 seymlht
REGLE 1 thelomoys
REGLE 2 syomoleht
REGLE 4 symleht
REGLE 1 mediocre
REGLE 2 ercoidem
REGLE 3 idemerco
REGLE 1 medeicro
REGLE 2 orciedem
REGLE 4 edemorci
REGLE 1 effet
REGLE 2 teffe
REGLE 3 tee
REGLE 1 effet
REGLE 2 teffe
REGLE 4 tee
REGLE 1 pierre
REGLE 2 erreip
REGLE 3 eiperr
REGLE 1 peerri
REGLE 2 irreep
REGLE 4 eepirr
REGLE 1 marquis
REGLE 2 siuqram
REGLE 3 siuram
REGLE 1 mirqaus
REGLE 2 suaqrim
REGLE 4 suarim
REGLE 1 montcalm
REGLE 2 mlactnom
REGLE 3 tnommlac
REGLE 1 mantcolm
REGLE 2 mloctnam
REGLE 4 tnammloc
REGLE 1 etait
REGLE 2 tiate
REGLE 3 tite
REGLE 1 atiet
REGLE 2 teita
REGLE 4 teta
REGLE 1 royaliste
REGLE 2 etsilayor
REGLE 3 etsiayor
REGLE 1 ryailesto
REGLE 2 otseliayr
REGLE 4 otseiayr
REGLE 1 alors
REGLE 2 srola
REGLE 3 srla
REGLE 1 olars
REGLE 2 sralo
REGLE 4 srlo
REGLE 1 celebre
REGLE 2 erbelec
REGLE 3 rblc
REGLE 1 celebre
REGLE 2 erbelec
REGLE 4 rblc
REGLE 1 toutes
REGLE 2 setuot
REGLE 3 uotset
REGLE 1 tuetos
REGLE 2 soteut
REGLE 4 eutsot
REGLE 1 grenouilles
REGLE 2 selliuonerg
REGLE 3 sellionerg
REGLE 1 gronuielles
REGLE 2 selleiunorg
REGLE 4 selleunorg
REGLE 1 turent
REGLE 2 tnerut
REGLE 3 ruttne
REGLE 1 terunt
REGLE 2 tnuret
REGLE 4 rettnu
REGLE 1 ecria
REGLE 2 airce
REGLE 3 aice
REGLE 1 icrae
REGLE 2 earci
REGLE 4 eaci
REGLE 1 tholomyes
REGLE 2 seymoloht
REGLE 3 seymlht
REGLE 1 thelomoys
REGLE 2 syomoleht
REGLE 4 symleht
REGLE 1 accent
REGLE 2 tnecca
REGLE 3 ccatne
REGLE 1 eccant
REGLE 2 tnacce
REGLE 4 ccetna
REGLE 1 homme
REGLE 2 emmoh
REGLE 3 eoh
REGLE 1 hemmo
REGLE 2 ommeh
REGLE 4 oeh
REGLE 1 ressaisit
REGLE 2 tisiasser
REGLE 3 tisisser
REGLE 1 rassiiset
REGLE 2 tesiissar
REGLE 4 tesssar
REGLE 1 empire
REGLE 2 eripme
REGLE 3 pmeeri
REGLE 1 emperi
REGLE 2 irepme
REGLE 4 pmeire
REGLE 1 remettez
REGLE 2 zettemer
REGLE 3 emerzett
REGLE 1 remettez
REGLE 2 zettemer
REGLE 4 emerzett
REGLE 1 stupeur
REGLE 2 rueputs
REGLE 3 rueuts
REGLE 1 stupuer
REGLE 2 reuputs
REGLE 4 reuuts
REGLE 1 accueille
REGLE 2 ellieucca
REGLE 3 lliucca
REGLE 1 eccauelli
REGLE 2 illeuacce
REGLE 4 illeacce
SND ee.05Vacdpt~ **CDSV\]adinqst **eeqquu5Dls{ 22JJaanntt*+08Rcehip  *0Daerstu aa*0\]ceinst eeettt**mm0<=PV[\blns| eee<<BBbbnnss#AOQSZfgklmz tt0<ae ss0W\_bdrtu ==0<^cehlmsty iinntt'*0BOevy pp*045QRanors **0Dinpt aa*025QRdhprs >acei LLeeoo./3KNdimnst oo)*0nsuv sss<<3;CL\^abehilnotu{ iii<<oo()*3Z\gmnprstvxz eeett)+/<ANRbdmn 00*D_eiprst *DEbcir Vcelnu{| aass$-.em **0Dinpt sss14AQSbefgkmnou sss00*.CDSV\imqru JJ*+/KLUelmnos jj/4@KL_amst iill*/:DKL_aejpr JJnn&*+/KLUaegmos eeee*06A]cimnrtu nnss!*0>CSV\fiot} ****00nnttlm sssPP0157QRU_ehnopr 4:Kevyz 00pp*4>Geimnrs} ""&0cdhp 668Vabeflmvz{ ee0Bsxy 00ee.=cprs PPP00rr#1EGWX_abcdeost ee0Bsxy &+@GHacfgr *.2`deinrs sss00*.CDSV\imqru EEPP01KTcdgimor eeerr*,4CGTXhilny aallnntt+<AEQSYZ^bdefgkmrsy *UYdeorstu ee*7:binor #46Eacdgnor 7\egopruwy ==0<^cehlmsty ss#V[aeiln{ PPll .01:A]aeknqrstu lllvv#.8:;WX[abceh *=anty */;befilmu aabb"&*6;FNdmor bb"'*.>acemno bb!"*+.GHWacehmo JJll+05AQRUeorst ccnnoo59>adehimv ee5BQRUirstxy rr*0CDSVW\_deipqst */;befilmu mmss*<ANR`eo ss",<GTX`behor aa.Ejotu JJll+05AQRUeorst ==0<^cehlmsty lllvv#.8:;WX[abceh JJLL+/3AKNQUcefgklmnopst ss.NW_abcdem &-0aimrsu PPnn-1<EVdeioprt{ ==0<^cehlmsty ee.7Y[cdimor ee0t eeiirr,GTXYhpt &-0aimrsu mm*./0:KLM`acjlnot tt0<ae PP14Uaeiorsty 00*lors ""*0bclr ttteeYiosu eeggll)/026<Znorsuxz tt,0<`enru >acei ==0<^cehlmsty ""<<ccZaentz ehior sss*02<D`aert ee*0\impr eeett%)36=cmnrz uu&05egrst cceell9>aiz{~
----------------
RCV Merci ! C'est exactement ce qu'il me fallait !
Voici ta récompense : 404CTF{--REDACTED--}
----------------

```