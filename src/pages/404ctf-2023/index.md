---
title: "404ctf-2023 - Summary"
slug: "/404ctf-2023"
date: "2023-05-01"
tags: ["404ctf", "404ctf 2023"]
---

Place: `221th`
Points: `7177`

![Solves](./solves.png)

Writeups
--------

- [Cloud] [Harpagon et le magot](/404ctf-2023/harpagon-et-le-magot) - 967pts
- [Programmation] [Des mots, des mots, des mots](/404ctf-2023/des-mots) - 704pts



