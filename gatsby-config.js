if (process.env.STAGING) {
  require("dotenv").config({
    path: `.env.${process.env.NODE_ENV}.staging`,
  })
} else {
  require("dotenv").config({
    path: `.env.${process.env.NODE_ENV}`,
  })
}

module.exports = {
  pathPrefix: process.env.CI_PROJECT_NAME || process.env.PATH_PREFIX || "/",
  siteMetadata: {
    title: `Kktus Writeups`,
    description: `Writeups of Kktus`,
    author: `@kktus`,
    siteUrl: `https://kktus.gitlab.io/writeups/`,
  },
  plugins: [
      `gatsby-plugin-sharp`,
      {
        resolve: `gatsby-source-filesystem`,
        options: {
          name: `pages`,
          path: `${__dirname}/src/pages`,
        },
      },
      {
        resolve: `gatsby-transformer-remark`,
        options: {
          plugins: [
            `gatsby-remark-prismjs`,
            `gatsby-remark-images`,
          ],
        },
      },
  ],
}
